from gitmachine import GitMachine

aliases = { 'upstream:': 'git://git.baserock.org/delta/',
            'qt.io:' : 'http://code.qt.io/qt/' }

ansible = {'lib/ansible/modules/core': { 'url': 'upstream:ansible-modules-core.git' },
           'v2/ansible/modules/core': { 'url': 'upstream:ansible-modules-core.git' },
           'v2/ansible/modules/extras': { 'url': 'upstream:ansible-modules-extras.git' },
           'lib/ansible/modules/extras': { 'url': 'upstream:ansible-modules-extras.git' }}

x = GitMachine('gits', 'tmp', aliases, 'http://git.baserock.org/tarballs')


qt5 = {'qtbase': { 'url': 'upstream:qt5/qtbase' },
       'qtsvg' : { 'url': 'upstream:qt5/qtsvg' },
       'qtdeclarative': { 'url': 'upstream:qt5/qtdeclarative',
                          'submodules': { 'tests/manual/v4/test262': { 'url': 'upstream:qt5/qtdeclarative-testsuites' } } },
       'qtactiveqt': { 'url': 'upstream:qt5/qtactiveqt' },
       'qtscript': { 'url' : 'upstream:qt5/qtscript' },
       'qtmultimedia': { 'url': 'upstream:qt5/qtmultimedia' },
       'qttools': { 'url': 'upstream:qt5/qttools' },
       'qtxmlpatterns': { 'url': 'upstream:qt5/qtxmlpatterns',
                          'submodules': { 'tests/auto/3rdparty/testsuites': { 'url': 'upstream:qt5/qtxmlpatterns-testsuites' } } },
       'qttranslations': { 'url': 'upstream:qt5/qttranslations' },
       'qtdoc': { 'url': 'upstream:qt5/qtdoc' },
       'qtrepotools': { 'url': 'upstream:qt5/qtrepotools' },
       'qtwebkit': { 'url': 'upstream:qt5/qtwebkit' },
       'qtwebkit-examples': { 'url': 'upstream:qt5/qtwebkit-examples' },
       'qtqa': { 'url': 'upstream:qt5/qtqa' },
       'qtlocation': { 'url': 'upstream:qt5/qtlocation' },
       'qtsensors': { 'url': 'upstream:qt5/qtsensors' },
       'qtsystems': { 'url': 'qt.io:qtsystems' },
       'qtfeedback': { 'url': 'qt.io:qtfeedback' },
       'qtdocgallery': { 'url': 'qt.io:qtdocgallery' },
       'qtpim': { 'url': 'qt.io:qtpim' },
       'qtwayland': { 'url': 'upstream:qt5/qtwayland' },
       'qtconnectivity': { 'url': 'upstream:qt5/qtconnectivity' },
       'qt3d': { 'url': 'upstream:qt5/qt3d' },
       'qtwayland': { 'url': 'upstream:qt5/qtwayland' },
       'qtimageformats': { 'url': 'upstream:qt5/qtimageformats' },
       'qtquick1': { 'url': 'upstream:qt5/qtquick1' },
       'qtgraphicaleffects': { 'url': 'upstream:qt5/qtgraphicaleffects' },
       'qtquickcontrols' : { 'url': 'upstream:qt5/qtquickcontrols' },
       'qtserialbus': { 'url': 'qt.io:qtserialbus' },
       'qtserialport': { 'url': 'upstream:qt5/qtserialport' },
       'qtx11extras' : { 'url': 'qt.io:qtx11extras' },
       'qtmacextras' : { 'url': 'qt.io:qtmacextras' },
       'qtwinextras' : { 'url': 'qt.io:qtwinextras' },
       'qtandroidextras' : { 'url': 'qt.io:qtandroidextras' },
       'qtenginio' : { 'url' : 'qt.io:qtenginio' },
       'qtwebsockets' : { 'url': 'upstream:qt5/qtwebsockets' },
       'qtwebchannel' : { 'url': 'qt.io:qtwebchannel' },
       'qtwebengine': { 'url': 'qt.io:qtwebengine',
                        'submodules': { 'src/3rdparty' : { 'url': 'qt.io:qtwebengine-chromium' } } },
       'qtcanvas3d': { 'url': 'qt.io:qtcanvas3d' },
       'qtwebview': { 'url': 'qt.io:qtwebview' },
       'qtquickcontrols2': { 'url': 'qt.io:qtquickcontrols2' },
       'qtpurchasing' : { 'url': 'qt.io:qtpurchasing' },
       'qtcharts': { 'url': 'qt.io:qtcharts' },
       'qtdatavis3d': { 'url': 'qt.io:qtdatavis3d' },
       'qtvirtualkeyboard' : { 'url': 'qt.io:qtvirtualkeyboard' },
       'qtgamepad': { 'url': 'qt.io:qtgamepad' },
       'qtscxml' : { 'url' : 'qt.io:qtscxml' },
       'qtspeech' : { 'url': 'qt.io:qtspeech' },
       'qtnetworkauth': { 'url': 'qt.io:qtnetworkauth' } }

x.arrange_into_folder('upstream:ansible', 'ebc8d48d34296fe010096f044e2b7591df37a622', ansible, 'ansible.build')
x.arrange_into_folder('upstream:qt5', '6fe694ef9e210e5d65a1179aef3c8681a20565b9', qt5, 'qt5.build')
